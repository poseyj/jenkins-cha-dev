const Code = require('code');   // assertion library
const Lab = require('lab');
const lab = exports.lab = Lab.script();

const suite = lab.suite;
const test = lab.test;
const expect = Code.expect;

const add = require("../src").add;

suite('add', () => {

    test('returns true when 1 + 1 equals 2', (done) => {
        expect(add(1, 1)).to.equal(2);
        done();
    });
});